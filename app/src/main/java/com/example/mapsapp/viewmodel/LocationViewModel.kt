package com.example.mapsapp.viewmodel

import android.content.ContentValues.TAG
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mapsapp.model.Location
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class LocationViewModel: ViewModel() {

    var data = MutableLiveData<MutableList<Location>>()
    var searchData = MutableLiveData<MutableList<Location>>()
    private val db = FirebaseFirestore.getInstance()
    private val storageRef = FirebaseStorage.getInstance().reference
    var currentLocation = MutableLiveData<Location>()
    lateinit var loggedUser: String
    lateinit var mGoogleSignInClient: GoogleSignInClient

    var imageUri: Uri? = null
    var audioUri: Uri? = null

    lateinit var foundedLocation: Location
    var markerLatitude: Double = 0.0
    var markerLongitude: Double = 0.0
    var imageDid: Boolean = false
    var firstRunDid = false
    var selectedAudioDid = MutableLiveData<Boolean>()
    var selectedImageDid = MutableLiveData<Boolean>()
    lateinit var fileName: String

    var currentFragment: Int = 0
    var animateMarker = false
    var locationToAnimateLongitude: Double = 0.0
    var locationToAnimateLatitude: Double = 0.0

    fun addUserToDb(email: String) {
        db.collection("users").document(email).set(
            hashMapOf("email" to email)
        )
    }

    fun addLocationToDb(location: Location, email: String) {
        db.collection("users").document(email).collection("locations").add(location)
    }

    fun getUserLocationsFromDb() {
        val list = mutableListOf<Location>()
        CoroutineScope(Dispatchers.IO).launch {
            db.collection("users").document(loggedUser).collection("locations").get()
                .addOnSuccessListener { locations ->
                    for (location in locations.documents) {
                        val title = location.data!!.get("title").toString()
                        val longitude = location.data!!.get("longitude").toString().toDouble()
                        val latitude = location.data!!.get("latitude").toString().toDouble()
                        val image = location.data!!.get("image").toString()
                        val audio = location.data!!.get("audio").toString()
                        val category = location.data!!.get("category").toString()
                        val id = location.id

                        val marker = Location(id, title, latitude, longitude, image, audio, category)
                        list.add(marker)
                        data.postValue(list)
                    }
                }
        }
    }

    fun addImageToDb() {
        val formatter = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.getDefault())
        val now = Date()
        fileName = formatter.format(now)
        val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
        storage.putFile(imageUri!!)
            .addOnSuccessListener {
                Log.d(TAG, "Image uploaded!")
            }
            .addOnFailureListener {
                Log.d(TAG, "Image not uploaded!")
            }
    }

    fun addAudioToDb() {
        val storage = FirebaseStorage.getInstance().getReference("audios/$fileName")
        storage.putFile(audioUri!!)
            .addOnSuccessListener {
                Log.d(TAG, "Audio uploaded!")
            }
            .addOnFailureListener {
                Log.d(TAG, "Audio not uploaded!")
            }
    }

    fun deleteCurrentMarker() {
        db.collection("users").document(loggedUser).collection("locations")
            .document(currentLocation.value!!.id!!).delete()

        storageRef.child("images/${currentLocation.value!!.image}").delete()
        storageRef.child("audios/${currentLocation.value!!.audio}").delete()

        if (data.value!!.size == 1) data.value!!.removeAll { true }
    }

    fun editMarkerTitle(newTitle: String) {
        val documentRef = db.collection("users").document(loggedUser).collection("locations").document(currentLocation.value!!.id!!)
        val updateTitle = mapOf<String, Any>("title" to newTitle)
        documentRef.update(updateTitle)
    }

    fun editMarkerCategory(newCategory: String) {
        val documentRef = db.collection("users").document(loggedUser).collection("locations").document(currentLocation.value!!.id!!)
        val updateCategory = mapOf<String, Any>("category" to newCategory)
        documentRef.update(updateCategory)
    }

    fun editMarkerAudio() {
        val storage = FirebaseStorage.getInstance().getReference("audios/$fileName")
        storage.putFile(audioUri!!)
            .addOnSuccessListener {
                Log.d(TAG, "Audio uploaded!")
                val documentRef = db.collection("users").document(loggedUser).collection("locations").document(currentLocation.value!!.id!!)
                val updateAudio = mapOf<String, Any>("audio" to fileName)
                documentRef.update(updateAudio)
                selectedAudioDid.value = true
            }
            .addOnFailureListener {
                Log.d(TAG, "Audio not uploaded!")
            }
    }

    fun editMarkerImage() {
        val storage = FirebaseStorage.getInstance().getReference("images/$fileName")
        storage.putFile(imageUri!!)
            .addOnSuccessListener {
                Log.d(TAG, "Image uploaded!")
                val documentRef = db.collection("users").document(loggedUser).collection("locations").document(currentLocation.value!!.id!!)
                val updateImage = mapOf<String, Any>("image" to fileName)
                documentRef.update(updateImage)
                selectedImageDid.value = true
            }
            .addOnFailureListener {
                Log.d(TAG, "Image not uploaded!")
            }
    }

    fun findLocationInDbById(id: String) {
        db.collection("users").document(loggedUser).collection("locations").document(id)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val document = task.result
                    if (document != null && document.exists()) {
                        val title = document.getString("title") ?: ""
                        val longitude = document.getDouble("longitude") ?: 0.0
                        val latitude = document.getDouble("latitude") ?: 0.0
                        val image = document.getString("image") ?: ""
                        val audio = document.getString("audio") ?: ""
                        val category = document.getString("category") ?: ""
                        foundedLocation = Location(id, title, latitude, longitude, image, audio, category)
                        currentLocation.postValue(foundedLocation)
                    }
                }
                else {
                    Log.d(TAG, "Error getting document: ", task.exception)
                }
            }
    }

    fun searchByTitle(title: String) {
        val db = FirebaseFirestore.getInstance()

        db.collection("users").document(loggedUser).collection("locations")
            .whereGreaterThanOrEqualTo("title", title)
            .get()
            .addOnSuccessListener { documents ->
                val foundLocationList = mutableListOf<Location>()
                for (document in documents) {
                    val locationTitle = document.data.get("title").toString()
                    val longitude = document.data.get("longitude").toString().toDouble()
                    val latitude = document.data.get("latitude").toString().toDouble()
                    val image = document.data.get("image").toString()
                    val audio = document.data.get("audio").toString()
                    val category = document.data.get("category").toString()
                    val id = document.id

                    val location = Location(id, locationTitle, latitude, longitude, image, audio, category)
                    foundLocationList.add(location)
                    searchData.postValue(foundLocationList)
                }
            }
    }
}