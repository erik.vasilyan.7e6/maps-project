package com.example.mapsapp.view.fragments

import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mapsapp.R
import com.example.mapsapp.databinding.FragmentMarkerListBinding
import com.example.mapsapp.model.Location
import com.example.mapsapp.model.LocationAdapter
import com.example.mapsapp.view.listeners.OnClickListener
import com.example.mapsapp.viewmodel.LocationViewModel

class MarkerListFragment : Fragment(), OnClickListener {

    private lateinit var binding: FragmentMarkerListBinding
    private lateinit var locationAdapter: LocationAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMarkerListBinding.inflate(layoutInflater)

        locationAdapter = LocationAdapter(ArrayList(), this)
        linearLayoutManager = LinearLayoutManager(context)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = locationAdapter
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        viewModel.currentFragment = 1

        viewModel.getUserLocationsFromDb()

        viewModel.data.observe(viewLifecycleOwner) {
            setupRecyclerView(it)
        }

        viewModel.searchData.observe(viewLifecycleOwner) {
            setupRecyclerView(it)
        }

        if (binding.addMarkerFragmentContainerFl.visibility == View.VISIBLE) {
            binding.addMarkerFragmentContainerFl.visibility = View.GONE
        }

        binding.markerListAddMarkerButton.setOnClickListener {
            binding.markerListAddMarkerButton.visibility = View.GONE
            binding.addMarkerFragmentContainerFl.visibility = View.VISIBLE
            showAddMarkerFragmentInWindow()
        }

        binding.searchSv.setOnClickListener {
            binding.searchSv.isIconified = false
        }

        binding.searchSv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText.toString() != "") {
                    viewModel.searchByTitle(newText.toString())
                }
                else {
                    try {
                        setupRecyclerView(viewModel.data.value!!)
                    } catch (e: java.lang.NullPointerException) {
                        Log.d(TAG, "Markers list size is 0")
                    }
                }
                return false
            }
        })

        if (viewModel.imageDid) {
            viewModel.imageDid = false
            showAddMarkerFragmentInWindow()
        }

        binding.searchSv.setOnSearchClickListener {
            val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(binding.searchSv.windowToken, 0)
        }
    }

    override fun onClick(location: Location) {
        val locationViewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
        locationViewModel.currentLocation.postValue(location)

        findNavController().navigate(R.id.action_markerListFragment_to_detailFragment)
    }

    private fun setupRecyclerView(locations: MutableList<Location>) {
        locationAdapter = LocationAdapter(locations, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = locationAdapter
        }
    }

    private fun showAddMarkerFragmentInWindow() {
        binding.addMarkerFragmentContainerFl.visibility = View.VISIBLE
        val fragmentManager: FragmentManager = childFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.add_marker_fragment_container_fl, AddMarkerFragment())
        fragmentTransaction.commit()
    }
}