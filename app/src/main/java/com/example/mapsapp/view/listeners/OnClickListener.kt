package com.example.mapsapp.view.listeners

import com.example.mapsapp.model.Location

interface OnClickListener {
    fun onClick(location: Location)
}