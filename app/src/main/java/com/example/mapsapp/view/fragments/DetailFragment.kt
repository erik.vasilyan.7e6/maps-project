package com.example.mapsapp.view.fragments

import android.app.Activity
import android.app.AlertDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.Rotate
import com.bumptech.glide.request.RequestOptions
import com.example.mapsapp.R
import com.example.mapsapp.databinding.FragmentDetailBinding
import com.example.mapsapp.viewmodel.LocationViewModel
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    lateinit var mediaPlayer: MediaPlayer
    lateinit var seekBar: SeekBar
    lateinit var playButton: ImageView
    private val handler = Handler(Looper.getMainLooper())
    private lateinit var audioUri: Uri
    private lateinit var imageUri: Uri

    private val updateSeekBar = object : Runnable {
        override fun run() {
            if (mediaPlayer.isPlaying) {
                seekBar.progress = mediaPlayer.currentPosition
                handler.postDelayed(this, 1000)
                mediaPlayer.setOnCompletionListener {
                    stopUpdatingSeekBar()
                    seekBar.progress = 0
                    playButton.setImageResource(R.drawable.pause_audio_icon)
                }
            }
        }
    }

    private var resultLauncherImage = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
                imageUri = data.data!!
                viewModel.imageUri = imageUri
                Glide.with(this)
                    .load(imageUri)
                    .into(binding.markerDetailImageIv)
                viewModel.editMarkerImage()
            }
        }
    }

    private var resultLauncherAudio = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
                audioUri = data.data!!
                viewModel.audioUri = audioUri
                viewModel.editMarkerAudio()
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        mediaPlayer = MediaPlayer()
        seekBar = binding.seekBar
        playButton = binding.playButton
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        playButton.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                stopUpdatingSeekBar()
                playButton.setImageResource(R.drawable.play_audio_icon)
            } else {
                mediaPlayer.start()
                startUpdatingSeekBar()
                playButton.setImageResource(R.drawable.pause_audio_icon)
            }
        }

        viewModel.currentLocation.observe(viewLifecycleOwner) { location ->
            binding.markerDetailTitleTv.text = location.title
            binding.markerDetailCategoryTv.text = location.category

            val imageStorage = FirebaseStorage.getInstance().reference.child("images/${location.image}")
            val imageLocalFile = File.createTempFile("temp", "jpeg")

            imageStorage.getFile(imageLocalFile)
                .addOnSuccessListener {
                    val imageBitmap = BitmapFactory.decodeFile(imageLocalFile.absolutePath)
                    Glide.with(requireContext())
                        .load(imageBitmap)
                        .apply(RequestOptions().transform(Rotate(90)))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(binding.markerDetailImageIv)
                }
                .addOnFailureListener {
                    Log.d(TAG,"Error downloading image!")
                }

            getAudioFromDb()
        }

        binding.deleteMarkerButton.setOnClickListener {
            viewModel.deleteCurrentMarker()
            Toast.makeText(context, "Marker deleted successfully!", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_detailFragment_to_markerListFragment)
        }

        binding.editTitleIv.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit, null)
            val builder = AlertDialog.Builder(context)
            builder.setTitle("New title for the marker:")
            val editText = dialogView.findViewById<EditText>(R.id.dialog_edit_title_et)
            editText.visibility = View.VISIBLE
            builder.setView(dialogView)
            builder.setPositiveButton("OK") { _, _ ->
                val newTitle = editText.text.toString()
                if (newTitle != "") {
                    viewModel.editMarkerTitle(newTitle)
                    binding.markerDetailTitleTv.text = newTitle
                }
            }
            builder.setNegativeButton("Cancel") { dialogInterface, _ ->
                dialogInterface.cancel()
            }
            builder.show()
        }

        binding.editCategoryIv.setOnClickListener {
            val dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_edit, null)
            val builder = AlertDialog.Builder(context)
            builder.setTitle("New category for the marker:")
            val spinner = dialogView.findViewById<AbsSpinner>(R.id.dialog_categories_spinner)
            spinner.visibility = View.VISIBLE
            builder.setView(dialogView)
            builder.setPositiveButton("OK") { _, _ ->
                val newCategory = spinner.selectedItem.toString()
                viewModel.editMarkerCategory(newCategory)
                binding.markerDetailCategoryTv.text = newCategory
            }
            builder.setNegativeButton("Cancel") { dialogInterface, _ ->
                dialogInterface.cancel()
            }
            builder.show()
        }

        binding.editImageIv.setOnClickListener {
            viewModel.fileName = viewModel.currentLocation.value?.image.toString()
            selectImage()
        }

        binding.editAudioIv.setOnClickListener {
            viewModel.fileName = viewModel.currentLocation.value?.audio.toString()
            selectAudio()
        }

        binding.startEditingButton.setOnClickListener {
            if (binding.startEditingButton.drawable.constantState == ContextCompat.getDrawable(requireContext(), R.drawable.can_edit)?.constantState) {
                binding.startEditingButton.setImageResource(R.drawable.no_edit)
                binding.editTitleIv.visibility = View.VISIBLE
                binding.editCategoryIv.visibility = View.VISIBLE
                binding.editAudioIv.visibility = View.VISIBLE
                binding.editImageIv.visibility = View.VISIBLE
            }
            else {
                binding.startEditingButton.setImageResource(R.drawable.can_edit)
                binding.editTitleIv.visibility = View.GONE
                binding.editCategoryIv.visibility = View.GONE
                binding.editAudioIv.visibility = View.GONE
                binding.editImageIv.visibility = View.GONE
            }
        }

        binding.goToMarkerIv.setOnClickListener {
            viewModel.locationToAnimateLongitude = viewModel.currentLocation.value!!.longitude
            viewModel.locationToAnimateLatitude = viewModel.currentLocation.value!!.latitude
            viewModel.animateMarker = true
            findNavController().navigate(R.id.action_detailFragment_to_mapFragment)
        }

        binding.previousPageIv.setOnClickListener {
            if (viewModel.currentFragment == 0) {
                findNavController().navigate(R.id.action_detailFragment_to_mapFragment)
            }
            if (viewModel.currentFragment == 1) {
                findNavController().navigate(R.id.action_detailFragment_to_markerListFragment)
            }
        }

        viewModel.selectedAudioDid.observe(viewLifecycleOwner) {
            if (it) {
                mediaPlayer = MediaPlayer()
                getAudioFromDb()
            }
        }
    }

    private fun getAudioFromDb() {
        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        val audioStorage = FirebaseStorage.getInstance().reference.child("audios/${viewModel.currentLocation.value!!.audio}")
        val audioLocalFile = File.createTempFile("audio", "mpeg")
        audioStorage.getFile(audioLocalFile).addOnSuccessListener {
            mediaPlayer = MediaPlayer()
            mediaPlayer.setDataSource(audioLocalFile.path)
            mediaPlayer.prepare()
            seekBar.max = mediaPlayer.duration
        }
    }

    private fun startUpdatingSeekBar() {
        handler.postDelayed(updateSeekBar, 1000)
    }

    private fun stopUpdatingSeekBar() {
        handler.removeCallbacks(updateSeekBar)
    }

    private fun selectAudio() {
        val intent = Intent()
        intent.type = "audio/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncherAudio.launch(intent)
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncherImage.launch(intent)
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        stopUpdatingSeekBar()
        mediaPlayer.reset()
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
    }
}