package com.example.mapsapp.model

import android.content.ContentValues
import android.content.Context
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.Rotate
import com.bumptech.glide.request.RequestOptions
import com.example.mapsapp.R
import com.example.mapsapp.databinding.ItemLocationBinding
import com.example.mapsapp.view.listeners.OnClickListener
import com.google.firebase.storage.FirebaseStorage
import java.io.File

class LocationAdapter(private var locations: MutableList<Location>, private val listener: OnClickListener):
    RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    private lateinit var context: Context

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemLocationBinding.bind(view)
        fun setListener(location: Location){
            binding.root.setOnClickListener {
                listener.onClick(location)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_location, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return locations.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val location = locations[position]

        val imageStorage = FirebaseStorage.getInstance().reference.child("images/${location.image}")
        val imageLocalFile = File.createTempFile("temp", "jpeg")

        imageStorage.getFile(imageLocalFile)
            .addOnSuccessListener {
                val imageBitmap = BitmapFactory.decodeFile(imageLocalFile.absolutePath)
                with(holder){
                    setListener(location)
                    Glide.with(context)
                        .load(imageBitmap)
                        .apply(RequestOptions().transform(Rotate(90)))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(binding.locationIv)
                }
            }
            .addOnFailureListener {
                Log.d(ContentValues.TAG,"Error downloading image!")
            }

        with(holder){
            setListener(location)
            binding.locationTv.text = location.title
            binding.categoryTv.text = location.category
        }
    }
}