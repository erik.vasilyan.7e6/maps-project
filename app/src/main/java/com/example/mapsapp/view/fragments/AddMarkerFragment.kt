package com.example.mapsapp.view.fragments

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.mapsapp.R
import com.example.mapsapp.databinding.FragmentAddMarkerBinding
import com.example.mapsapp.model.Location
import com.example.mapsapp.view.activities.MainActivity
import com.example.mapsapp.viewmodel.LocationViewModel

class AddMarkerFragment : Fragment() {

    private lateinit var binding: FragmentAddMarkerBinding
    private lateinit var imageUri: Uri
    private lateinit var audioUri: Uri

    private var resultLauncherImage = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
                imageUri = data.data!!
                viewModel.imageUri = imageUri
                Glide.with(this)
                    .load(imageUri)
                    .into(binding.locationIv)
            }
        }
    }

    private var resultLauncherAudio = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
                audioUri = data.data!!
                viewModel.audioUri = audioUri
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentAddMarkerBinding.inflate(layoutInflater)

        val mainActivity = requireActivity() as MainActivity
        mainActivity.supportActionBar?.show()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        if (viewModel.imageUri != null) {
            imageUri = viewModel.imageUri!!

            Glide.with(this)
                .load(imageUri)
                .into(binding.locationIv)
        }
        else binding.locationIv.setImageResource(R.drawable.image_icon)

        binding.galeryButton.setOnClickListener {
            selectImage()
        }

        binding.takePhotoButton.setOnClickListener {
            if (viewModel.currentFragment == 0) {
                findNavController().navigate(R.id.action_mapFragment_to_cameraFragment)
            }
            if (viewModel.currentFragment == 1) {
                findNavController().navigate(R.id.action_markerListFragment_to_cameraFragment)
            }
        }

        binding.audioButton.setOnClickListener {
            selectAudio()
        }

        binding.addMarkerButton.setOnClickListener {
            if (!::audioUri.isInitialized) Toast.makeText(context, "Select audio!", Toast.LENGTH_SHORT).show()
            if (!::imageUri.isInitialized) Toast.makeText(context, "Select image!", Toast.LENGTH_SHORT).show()
            if (binding.markerTitleEt.text.toString() == "") Toast.makeText(context, "Insert title!", Toast.LENGTH_SHORT).show()
            if (::audioUri.isInitialized && ::imageUri.isInitialized && binding.markerTitleEt.text.toString() != "") {
                viewModel.addImageToDb()
                viewModel.addAudioToDb()
                val location = Location(null, binding.markerTitleEt.text.toString(), viewModel.markerLatitude, viewModel.markerLongitude, viewModel.fileName, viewModel.fileName, binding.categoriesSpinner.selectedItem.toString())
                viewModel.addLocationToDb(location, viewModel.loggedUser)
                viewModel.getUserLocationsFromDb()
                closeAddMarkerCard()
            }
        }

        binding.closeAddMarkerFragmentIv.setOnClickListener {
            closeAddMarkerCard()
        }

        binding.markerTitleEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                // Submit button clicked, do something here
                val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(binding.markerTitleEt.windowToken, 0)
                true
            } else false
        }
    }

    private fun closeAddMarkerCard() {
        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        val cardView: CardView = requireActivity().findViewById(R.id.add_marker_fragment_container_fl)
        cardView.removeAllViews()
        cardView.visibility = View.GONE

        if (viewModel.currentFragment == 0) {
            val mapAddMarkerButton: ImageView = requireActivity().findViewById(R.id.add_marker_button)
            val goToMyLocationButton: ImageView = requireActivity().findViewById(R.id.go_to_my_location_button)
            mapAddMarkerButton.visibility = View.VISIBLE
            goToMyLocationButton.visibility = View.VISIBLE
        }
        else if (viewModel.currentFragment == 1) {
            val markerListAddMarkerButton: ImageView = requireActivity().findViewById(R.id.marker_list_add_marker_button)
            markerListAddMarkerButton.visibility = View.VISIBLE
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncherImage.launch(intent)
    }

    private fun selectAudio() {
        val intent = Intent()
        intent.type = "audio/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncherAudio.launch(intent)
    }
}