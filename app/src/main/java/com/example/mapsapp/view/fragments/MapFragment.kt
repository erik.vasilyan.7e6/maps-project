package com.example.mapsapp.view.fragments

import android.Manifest
import android.content.ContentValues.TAG
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.mapsapp.R
import com.example.mapsapp.databinding.FragmentMapBinding
import com.example.mapsapp.viewmodel.LocationViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions


class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val requestCodeLocation = 100
    private lateinit var binding: FragmentMapBinding
    private lateinit var map: GoogleMap
    private var isMapReady = false
    private val handler = Handler(Looper.getMainLooper())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentMapBinding.inflate(layoutInflater)
        createMap()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        viewModel.currentFragment = 0
        viewModel.getUserLocationsFromDb()

        viewModel.data.observe(viewLifecycleOwner) {
            if (it.size != 0 && ::map.isInitialized) showMarkers(it)
        }

        handler.postDelayed({
            val menuUserTextView = requireActivity().findViewById<TextView>(R.id.user_tv)
            menuUserTextView?.text = viewModel.loggedUser
        }, 100)

        binding.addMarkerButton.setOnClickListener {
            goToCurrentLocation()
            binding.addMarkerButton.visibility = View.GONE
            binding.goToMyLocationButton.visibility = View.GONE
            showAddMarkerFragmentInWindow()
        }

        binding.goToMyLocationButton.setOnClickListener {
            goToCurrentLocation()
        }

        if (viewModel.imageDid) {
            viewModel.imageDid = false
            showAddMarkerFragmentInWindow()
        }

        if (binding.addMarkerFragmentContainerFl.visibility == View.VISIBLE) {
            binding.addMarkerButton.visibility = View.GONE
            binding.goToMyLocationButton.visibility = View.GONE
        }

        viewModel.currentLocation.observe(viewLifecycleOwner) {
            Log.d(TAG, "Observe current location: $it")
        }
    }

    private fun showMarkers(locations: MutableList<com.example.mapsapp.model.Location>) {
        for (location in locations) {
            createMarker(location)
        }
    }

    private fun showAddMarkerFragmentInWindow() {
        binding.addMarkerFragmentContainerFl.visibility = View.VISIBLE
        val fragmentManager: FragmentManager = childFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.add_marker_fragment_container_fl, AddMarkerFragment())
        fragmentTransaction.commit()
    }

    private fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        val style = MapStyleOptions.loadRawResourceStyle(requireContext(), R.raw.map_style)
        mapFragment?.getMapAsync(this)
        mapFragment?.getMapAsync { googleMap ->
            // Set the map style options to the Google Maps object
            googleMap.uiSettings.isMyLocationButtonEnabled = false
            googleMap.setMapStyle(style)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        map = googleMap
        isMapReady = true
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        enableLocation()

        if (!viewModel.animateMarker && !viewModel.firstRunDid) {
            viewModel.firstRunDid = true
            goToCurrentLocation()
        }
        else if (viewModel.animateMarker && viewModel.firstRunDid) {
            val coordinates = LatLng(viewModel.locationToAnimateLatitude, viewModel.locationToAnimateLongitude)
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f), 3000, null)
            viewModel.animateMarker = false
        }
        else {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(0.0, 0.0), 1f), 3000, null)
        }

        map.setOnMapLongClickListener {
            binding.addMarkerButton.visibility = View.GONE
            binding.goToMyLocationButton.visibility = View.GONE
            viewModel.markerLatitude = it.latitude
            viewModel.markerLongitude = it.longitude
            showAddMarkerFragmentInWindow()
        }

        map.setOnMarkerClickListener {
            // find marker in db and set it to currentLocation that is in viewModel
            viewModel.findLocationInDbById(it.snippet!!)
            findNavController().navigate(R.id.action_mapFragment_to_detailFragment)
            true
        }
    }

    private fun createMarker(location: com.example.mapsapp.model.Location){
        val coordinates = LatLng(location.latitude, location.longitude)

        val markerOptions = MarkerOptions().position(coordinates).title(location.title).snippet(location.id)
        map.addMarker(markerOptions)
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableLocation(){
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) { return }
            map.isMyLocationEnabled = true
        }
        else {
            requestLocationPermission()
        }
    }

    private fun requestLocationPermission(){
        if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)){
            Toast.makeText(requireContext(), "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització", Toast.LENGTH_SHORT).show()
        }
        else{
            ActivityCompat.requestPermissions(requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCodeLocation)
        }
    }

     private fun goToCurrentLocation() {
        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]

        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) { return }
        map.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                val latitude = location.latitude
                val longitude = location.longitude
                viewModel.markerLatitude = latitude
                viewModel.markerLongitude = longitude

                Log.d(TAG, "CURRENT LOCATION: $latitude, $longitude")

                val coordinates = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f), 3000, null)
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode){
            requestCodeLocation -> {
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (ActivityCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            requireContext(),
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) { return }
                    map.isMyLocationEnabled = true
                }
                else {
                    Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if (!isLocationPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) { return }
            map.isMyLocationEnabled = false
            Toast.makeText(requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT).show()
        }
    }
}