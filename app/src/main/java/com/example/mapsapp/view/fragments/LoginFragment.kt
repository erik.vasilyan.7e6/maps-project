package com.example.mapsapp.view.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.mapsapp.R
import com.example.mapsapp.databinding.FragmentLoginBinding
import com.example.mapsapp.viewmodel.LocationViewModel
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mAuth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(requireContext(), gso)

        mAuth = FirebaseAuth.getInstance()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val drawerLayout: DrawerLayout = requireActivity().findViewById(R.id.drawer_layout)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.START)

        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
        viewModel.mGoogleSignInClient = mGoogleSignInClient

        val user = mAuth.currentUser
        if (user != null) {
            viewModel.loggedUser = user.email!!
            findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
        }

        binding.passwordEt.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                // Submit button clicked, do something here
                val inputMethodManager = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(binding.passwordEt.windowToken, 0)
                true
            } else false
        }

        binding.loginButton.setOnClickListener {
            val emailEtText = binding.emailEt.text.toString()
            val passwordEtText = binding.passwordEt.text.toString()
            if (passwordEtText.length < 6) Toast.makeText(context, "Password needs to be at least 6 characters", Toast.LENGTH_SHORT).show()
            else if (emailEtText.isNotEmpty()) login(emailEtText, passwordEtText)
        }

        binding.signUpButton.setOnClickListener {
            val emailEtText = binding.emailEt.text.toString()
            val passwordEtText = binding.passwordEt.text.toString()
            if (passwordEtText.length < 6) Toast.makeText(context, "Password needs to be at least 6 characters", Toast.LENGTH_SHORT).show()
            else if (emailEtText.isNotEmpty()) signUp(emailEtText, passwordEtText)
        }

        binding.googleSignInButton.setOnClickListener {
            signInWithGoogle()
        }
    }

    private fun signInWithGoogle() {
        val signInIntent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                val idToken = account?.idToken

                val credential = GoogleAuthProvider.getCredential(idToken, null)
                mAuth.signInWithCredential(credential).addOnCompleteListener(requireActivity()) { loginTask ->
                    if (loginTask.isSuccessful) {
                        // User successfully signed in with Firebase
                        val user = mAuth.currentUser
                        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
                        viewModel.loggedUser = user?.email!!
                        findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
                    } else {
                        // Handle sign-in error
                    }
                }
            } catch (e: ApiException) {
                // Handle sign-in error
            }
        }
    }

    private fun signUp(email: String, password: String) {
        val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
        FirebaseAuth.getInstance().
        createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if(it.isSuccessful) {
                    viewModel.addUserToDb(email)
                    val emailLogged = it.result?.user?.email
                    viewModel.loggedUser = emailLogged!!
                    findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
                }
                else{
                    Toast.makeText(context, "Error al registrar l'usuari", Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun login(email: String, password: String) {
        FirebaseAuth.getInstance().
        signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if(it.isSuccessful){
                    val emailLogged = it.result?.user?.email
                    val viewModel = ViewModelProvider(requireActivity())[LocationViewModel::class.java]
                    viewModel.loggedUser = emailLogged!!
                    findNavController().navigate(R.id.action_loginFragment_to_mapFragment)
                }
                else{
                    Toast.makeText(context, "Error al fer login", Toast.LENGTH_SHORT).show()
                }
            }
    }

    companion object {
        private const val RC_SIGN_IN = 123
    }
}