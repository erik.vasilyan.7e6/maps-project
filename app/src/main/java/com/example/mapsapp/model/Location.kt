package com.example.mapsapp.model

data class Location(
    var id: String?=null,
    var title: String,
    val latitude: Double,
    val longitude: Double,
    val image: String,
    val audio: String,
    val category: String
    )
